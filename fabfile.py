# -*- coding: utf-8 -*-
'''stuf fabfile'''

from fabric.api import prompt, local, settings, env, lcd

regup = './setup.py register sdist --format=bztar,zip upload'
nodist = 'rm -rf dist'
sphinxup = './setup.py upload_sphinx'


def getversion(fname):
    '''
    Get the __version__ without importing.
    '''
    for line in open(fname):
        if line.startswith('__version__'):
            return '%s.%s.%s' % eval(line[13:])


def tag():
    with settings(warn_only=True):
        local('git tag "%s"' % getversion('stuf/__init__.py'))


def _test(val):
    truth = val in ['py26', 'py27', 'py32', 'py34']
    if truth is False:
        raise KeyError(val)
    return val


def tox():
    '''test stuf'''
    local('tox')


def docs():
    with lcd('docs/'):
        local('make clean')
        local('make html')
        local('make linkcheck')
        local('make doctest')


def tox_recreate():
    '''recreate stuf test env'''
    prompt(
        'Enter testenv: [py26, py27, py32, py34]',
        'testenv',
        validate=_test,
    )
    local('tox --recreate -e %(testenv)s' % env)


def release():
    '''release stuf'''
#    docs()
    local(regup)
#    local(sphinxup)
    local(nodist)


def inplace():
    '''in-place stuf'''
#    docs()
    local('./setup.py sdist --format=gztar,zip upload')
#    local(sphinxup)
    local(nodist)
